#!/usr/bin/env Rscript

## Collect arguments
args <- commandArgs(TRUE)
 
## Default setting when no arguments passed
if(length(args) < 1) {
  args <- c("--help")
}

## Help section
if("--help" %in% args) {
  cat("
      TEQC Report
 
      Arguments:
      --samplename=[STRING]   - Name of the sample (Default:None)
      --targetsname=[STRING]   - Name of the capture target set (Default:None)
      --targetsfile=[FILE]   - BED file containing capture regions (Default:None)
      --bamfile=[FILE]   - Input BAM file (Default:None)
      --destdir=[DIR]   - Output directory (Default:None)
      --genome=[hg18/hg19]   - Set as hg18/19 (Default:hg19) - Set referencename and genomesize for other genomes
      --referencename=[STRING]   - Set for genomes other than hg18/19 (Default:None)
      --genomesize=[INT] - Set for genomes other than hg18/19 (Default:None)
      --pairedend=[T/F]   - logical indicating paired-end reads (Default:T)
      --help              - print this text
 
      Example:
      teqc --samplename=test1 --targetsname=SureSelectV5 --bamfile=test1.bam --destdir=./out --genome=hg19 \n\n")
 
  q(save="no")
}
 
## Parse arguments (we expect the form --arg=value)
parseArgs <- function(x) strsplit(sub("^--", "", x), "=")
argsDF <- as.data.frame(do.call("rbind", parseArgs(args)))
argsL <- as.list(as.character(argsDF$V2))
names(argsL) <- argsDF$V1
 
if(is.null(argsL$samplename)) {
    print ("You must supply a sample name using --samplename")
    q(save="no")
}
 
if(is.null(argsL$targetsname)) {
    print ("You must supply a name for the capture target set using --targetsname")
    q(save="no")
}
 
if(is.null(argsL$targetsfile)) {
    print ("You must supply a BED file with target regions using --targetsfile")
    q(save="no")
}

if(is.null(argsL$bamfile)) {
    print ("You must supply an input BAM file location using --bamfile")
    q(save="no")
}

if(is.null(argsL$destdir)) {
    print ("You must supply an ouput directory location using --destdir")
    q(save="no")
}

if(is.null(argsL$genome)) {
    if(is.null(argsL$referencename) || is.null(argsL$genomesize)) {
        print ("You must supply either the genome as hg18 or hg19 using --genome or a reference name and genome size using --referencename and --genomesize")
        q(save="no")
    }   
} else {

if(!(argsL$genome %in% c("hg18","hg19")))
{
    print ("The --genome arguement only accepts 'hg18' or 'hg19', for other genomes supply --referencename and --genomesize")
    q(save="no")
}

}

if(argsL$pairedend=="F" | argsL$pairedend=="FALSE") {
  argsL$pairedend = FALSE
} else {
    argsL$pairedend = TRUE
}


require(TEQC)

print("Loading targets file:")
print(argsL$targetsfile)

targs = get.targets(argsL$targetsfile)

print("Loading BAM file:")
print(argsL$bamfile)

bamdata=get.reads(argsL$bamfile, filetype='bam')

if(!is.null(argsL$genome))
{
TEQCreport(
    sampleName=argsL$samplename,
    targetsName=argsL$targetsname,
    destDir=argsL$destdir,
    reads=bamdata,
    pairedend=argsL$pairedend,
    targets=targs,
    genome=argsL$genome
    )
} else {
TEQCreport(
    sampleName=argsL$samplename,
    targetsName=argsL$targetsname,
    referenceName=argsL$referencename,
    genomesize=as.numeric(argsL$genomesize),
    destDir=argsL$destdir,
    reads=bamdata,
    pairedend=argsL$pairedend,
    targets=targs,
    )    
}
