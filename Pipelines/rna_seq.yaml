info:
  description: RNAseq QC and Counting from fastq files
  date:        10/01/2018
  arguments:
    sample_name:
      Name of the tumor sample
    star_out_dir:
      STAR Output directory
    kallisto_out_dir:
      Kallisto Output directory
    qc_out_dir:
      QC directory output path    
    kallisto_index:
      Filename for the kallisto index to be used for quantification. See /home/projects/cu_10027/data/genomes/kallisto
    kallisto_bsamples:
      Number of samples to use for kallisto bootstrapping 
    kallisto_bseed:
      Seed value to use for kallisto bootstrapping
    pizzly:
      Output the files required by Pizzly fusion detection (True/False)
    stranded:
      (fwd/rvs) Indicate strand specific reads as first read is forward (fwd) or first read is reverse (rvs)
    fragment-length:
      Estimated fragment length, not required for paired-end data
    fragment-sd:
      Estimated standard deviation of fragment length, not required for paired-end data
    fq1:
      FastQ file 1
    fq2:
      FastQ file 2
    SEPE:
      Single End (SE) or Paired End (PE) data
    bamRGheader:
      The read group line for the bam file. Read from FastQ by default
    refmodel:
      Transcript reference model in BED format (defaults to rseqc_model_bed in profile)
    starGenomeDir:
      Location of the STAR pre-processed genome files. Uses star_GRCh37_v19 from pipeline profile by default
    starChimSegMin:
      The minimum length of sequence present from each genomic region to call a chimeric read. Default:25
    tmp_dir:
      temporary folder
    threads:
      Number of parallel CPU threads
  defaults:
    tmp_dir: /scratch
    threads: 8
    fq2: ~
    normal_header: ~
    tumor_header: ~
    starGenomeDir: ~
    bamRGheader: ~
    starChimSegMin: ~
    kallisto_bseed: ~
    kallisto_bsamples: ~
    stranded: ~
    fragment-length: ~
    fragment-sd: ~
    pizzly: True
    refmodel: ~
items:
  - name: rseqc
    type: snippet
    arguments:
      --samplename: '%(sample_name)s'
      --input: [star, alignBam, {--out: '%(star_out_dir)s', --samplename: '%(sample_name)s'}]
      --outputdir: '%(qc_out_dir)s'
      --SEPE: '%(SEPE)s'
      --refmodel: '%(refmodel)s'
      --threads: '%(threads)s'
    dependencies:
      items:
        - name: star
          type: snippet
          arguments:
            --fq1: '%(fq1)s'
            --fq2: '%(fq2)s'
            --genomeDir: '%(starGenomeDir)s'
            --samplename: '%(sample_name)s'
            --out: '%(star_out_dir)s'
            --tmp: '%(tmp_dir)s'
            --threads: '%(threads)s'
            --chimSegMin: '%(starChimSegMin)s'
            --bamrg: '%(bamRGheader)s'
  - name: kallisto
    type: snippet
    arguments:
      --fastq1: '%(fq1)s'
      --fastq2: '%(fq2)s'
      --outdir: '%(kallisto_out_dir)s'
      --index: '%(kallisto_index)s'
      --bootstrap-samples: '%(kallisto_bsamples)s'
      --seed: '%(kallisto_bseed)s'
      --fusion: '%(pizzly)s'
      --stranded: '%(stranded)s'
      --fragment-length: '%(fragment-length)s'
      --sd: '%(fragment-sd)s'
      --threads: '%(threads)s'
  - name: fastqc
    type: snippet
    mute: True
    arguments:
      --fq1: '%(fq1)s'
      --fq2: '%(fq2)s'
      --out: '%(qc_out_dir)s'
      --tmp: '%(tmp_dir)s'
      --threads: '%(threads)s'