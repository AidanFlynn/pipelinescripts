#! /usr/bin/env python


"""

###############################
Aidan.Flynn@finsenlab.dk
###############################

runKallisto.py
Submits PBS jobs to runs Kallisto for a batch of samples taken from a tab separated list of samples and FASTQ files; FORMAT:SampleName OutDir FASTQ1 FASTQ2 

Usage:
    runKallisto.py --sample-list <FILE> [--index <FILE>] [--bootstrap <INT>] [--seed <INT>]  [--threads <INT>] [--nobias] ([--stf]|[--str]) [--plaintext] [--mail <mailAddress>]

Options:
  --sample-list 	FILE  Tab separated input file with samples and FASTQ locations
  --index FILE  	Kallisto index file (default: Human GRCh38.rel79)
  --bootstrap INT   	Number of bootstrap samples (default: 0)
  --seed INT  		Seed for the bootstrap sampling (default: 42)
  --threads INT  	Number of parallel threads (default: 1)
  --mail STRING  	Email address to send PBS notifications
  --nobias  		Do NOT perform sequence based bias correction
  --plaintext  		Output plaintext instead of HDF5
  --fstr  		Strand specific reads, first read forward (Default: No)
  --rstr  		Strand specific reads, first read reverse (Default: No)
  --help  		Show this screen
"""

from docopt import docopt
import sys
import csv
import re
import os
import subprocess
from time import sleep
arguments = docopt(__doc__)


##Sample input list
if arguments['--sample-list']:
	sampleFile = arguments['--sample-list']
	if not re.match('^/',sampleFile):
		sampleFile = os.path.join(os.getcwd(), sampleFile)
else:
	print('You must provide a tab separated list of samples and FASTQ file locations using the --sample-list option')
	exit()

##Index file	
if arguments['--index']:
	indexFile = arguments['--index']
	if re.match('^/',indexFile):
		indexFile =	os.path.join(os.getcwd(), indexFile)
else:
	indexFile = '/home/projects/cu_10027/data/genomes/kallisto/GRCh38/Homo_sapiens.GRCh38.rel79.cdna.all.kidx'
	print ('Using transcript index file GRCh38.rel79:' + indexFile)

## Apply Bias Correction
correctBias = True
if arguments['--nobias']:
	correctBias = False

##Bootstrap sampling
bootstrapSamples = 0	
if arguments['--bootstrap']:
	try:
		bootstrapSamples = int(arguments['--bootstrap'])
	except ValueError:
		print('Invalid integer value supplied to --bootstrap')
		exit()

##Bootstrap seed
bootstrapSeed=42
if arguments['--seed']:
	try:
		bootstrapSeed = int(arguments['--seed'])
	except ValueError:
		print('Invalid integer value supplied to --seed')
		exit()
		
##Output format
plainText = False
if arguments['--plaintext']:
	plainText = True

##Stranded data	
strandedFirstFwd = False
if arguments['--stf']:
	strandedFirstFwd = True
	
strandedFirstRvrs = False
if arguments['--str']:
	strandedFirstRvrs = True
	
if strandedFirstFwd and strandedFirstRvrs:
	print('You cannot specify both --stf and --str')
	exit()	

#Multi-threading	
threads = 1
if arguments['--threads']:
	try:
		threads = int(arguments['--threads'])
	except ValueError:
		print('Invalid integer value supplied to -j/--threads')
		exit()
		
#Multi-threading	
sendmail = False
userMail = ''
if arguments['--mail']:
	userMail = arguments['--mail']
	sendmail=True


with open(sampleFile,'r') as sfh:
	sampleReader=csv.reader(sfh,delimiter='\t')
	for sampleName, outDir, FASTQ1, FASTQ2 in sampleReader:
		#Create outdir
		if not re.match('^/',outDir):
			outDir = os.path.abspath(outDir)		
		if not os.path.exists(outDir):
			os.makedirs(outDir)
			
		PBSout = "#!/bin/sh \n"
		PBSout += "#PBS -N Kallisto_" + sampleName + " \n"

		##Resources
		PBSout += "#PBS -l walltime=1:00:00 \n"
		PBSout += "#PBS -l mem=6gb \n"
		PBSout += "#PBS -l nodes=1:ppn=" + str(threads) + " \n"
		
		##Standard outs
		PBSout += "#PBS -e %s/Kallisto_PBS_%s.e \n" % [outDir,sampleName]
		PBSout += "#PBS -o %s/Kallisto_PBS_%s.o \n" % [outDir,sampleName]
		
		## Mail notifications
		if sendmail:
			PBSout += '#PBS -M '+ userMail + " \n"
			PBSout += '#PBS -m ae \n'
        
		#Module
		PBSout += 'module load kallisto/0.43.1 \n'
		
		
		
		#Command
		PBSout += "kallisto quant" 
		
		#Arguments
		PBSout += " --index=" + indexFile
		
		PBSout += " --output-dir=" + outDir 
		
		if correctBias:
			PBSout += " --bias" 

		if bootstrapSamples > 0:
			PBSout += " --bootstrap-samples="+str(bootstrapSamples) 
			PBSout += " --seed="+str(bootstrapSeed)

		if strandedFirstFwd:
    	    PBSout += "--fr-stranded"
		
		if strandedFirstRvrs:
    	    PBSout += "--rf-stranded"
		
		PBSout += " --threads="+str(threads)
		
		PBSout += " " + FASTQ1 + " " + FASTQ2
		
		pbsFile = os.path.join(outDir, sampleName + "_kallisto.pbs")
		
		with open(pbsFile,'w') as pfh:
			pfh.write(PBSout)		
		subprocess.call(['qsub', pbsFile])
		sleep(0.5)
		print('Wrote and submitted:'+pbsFile)