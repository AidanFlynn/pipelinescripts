#module add bcftools/1.8
#module add vcflib/1.0.0-rc0-260-g5e3c
#module add vt/0.5772

input=$1
output=$(echo $1 | sed 's/.vcf.gz/.filter.vcf.gz/')

fasta=/home/databases/sanger/PanCancer/hs37d5/genome.fa
bed=/home/projects/cu_10027/data/exomes/agilent_bait/SureSelectClinicalExomeV1/S06588914_Regions_coord_only.bed

bcftools view -R $bed $input \
    | vcffilter -f "QUAL > 20 & QUAL / AO > 2 & SAF > 1 & SAR > 1 & RPR > 1 & RPL > 1" \
    | vt normalize -r $fasta -q - 2> /dev/null \
    | vcfstreamsort \
    | vcfuniqalleles \
    | bcftools view -Oz -o $output

