#! /usr/bin/env python
"""
###############################
joachim.weischenfeldt@gmail.com
###############################

bedpe_gene_annotate.py
Adds gene annotation.

Usage:
    bedpe_gene_annotate.py -v <BEDPE> [-g <genes>]  [-m <max_dist>]

Options:
  -h --help     Show this screen.
  -v --BEDPE    BEDPE SV file
  -g --genes    gene annotation in BED format (chr, start, end, name, strand). Will use gencode v19 without "chr" [default: /home/projects/cu_10027/data/gene_annotation/gencode/gencode.v19.protein_coding_gene.minimal.bed.gz]
  -m --max_dist max combined distance to consider between two breakpoints and a gene element   [default: 50000]
"""


from __future__ import print_function
from docopt import docopt
import sys
import csv
import re
import gzip
import pandas as pd
import numpy as np
from pybedtools import BedTool
arguments = docopt(__doc__)

bedpe_file = arguments['<BEDPE>']
fileOutAnno = re.sub('\.[^.]*$','', bedpe_file) + '.genes.txt'


gencode = ''
if arguments['<genes>']:
    gencode_file = arguments['<genes>']
else:
    gencode_file = '/home/projects/cu_10027/data/gene_annotation/gencode/gencode.v19.protein_coding_gene.minimal.bed.gz'
gencode = BedTool(gencode_file)

#if arguments['<max_dist>']:
#    max_distance = arguments['<max_dist>']

def annotate(df_bedpe, gencode_bed, max_distance = 50000):
    df_bedpe[["chrom1", "chrom2"]] = df_bedpe[["chrom1", "chrom2"]].astype(str)
    bed_header = ["chrom", "start", "end", "id", "score", "strand"]
    # split BEDPE file into BED file
    df_bed_tmp1 = df_bedpe.iloc[:,[0,1,2,6,7,8]]
    df_bed_tmp1.columns = bed_header
    df_bed_tmp2 = df_bedpe.iloc[:,[3,4,5,6,7,9]]
    df_bed_tmp2.columns = bed_header
    df_bed = df_bed_tmp1.append(df_bed_tmp2)
    df_bed_plusstrand = df_bed[df_bed.strand == "+"]
    df_bed_minusstrand = df_bed[df_bed.strand == "-"]
    plus_bed = BedTool.from_dataframe(df_bed_plusstrand)
    minus_bed = BedTool.from_dataframe(df_bed_minusstrand)
    plus_bed.sort().closest(gencode_bed.sort(), D="ref").saveas("sv_gene_plus.bed")
    minus_bed.sort().closest(gencode_bed.sort(), D="ref").saveas("sv_gene_minus.bed")
    df_sv_gene_plus = pd.read_table("sv_gene_plus.bed", header = None)
    df_sv_gene_minus = pd.read_table("sv_gene_minus.bed", header = None)
    df_sv_gene_bed = df_sv_gene_plus.append(df_sv_gene_minus).sort_values([0,1,2])
    df_sv_gene_bed.columns = bed_header + ['chrom_gene1', 'start_gene1', 'end_gene1', 'name_gene1', 'strand_gene1', 'dist_gene1']
    df_sv_gene_bed["chrom"] = df_sv_gene_bed["chrom"].astype(str)
    df_merge_tmp1 = pd.merge(df_bedpe, df_sv_gene_bed, left_on = ["chrom1", "start1", "id"], right_on = ["chrom", "start", "id"], how = "left")
    df_merge_tmp1.drop(["chrom", "start", "end", "score", "strand"], axis = 1, inplace = True)
    df_sv_gene_bed.columns = bed_header + ['chrom_gene2', 'start_gene2', 'end_gene2', 'name_gene2', 'strand_gene2', 'dist_gene2']
    df_merge_tmp2 = pd.merge(df_merge_tmp1, df_sv_gene_bed, left_on = ["chrom2", "start2", "id"], right_on = ["chrom", "start", "id"], how = "left")
    df_merge_tmp2.drop(["chrom", "start", "end", "score", "strand"], axis = 1, inplace = True)
    df_bedpe_anno = df_merge_tmp2.copy()
    df_bedpe_anno["fusion_orient"] = "."
    df_bedpe_anno["fusion"] = "."
    df_bedpe_anno["fusion_orient"] = np.where((df_bedpe_anno["dist_gene1"] + df_bedpe_anno["dist_gene2"] < max_distance) & \
    (df_bedpe_anno["name_gene1"] != df_bedpe_anno["name_gene2"]) & (~df_bedpe_anno["name_gene1"].isnull()) & (~df_bedpe_anno["name_gene2"].isnull()), 
    df_bedpe_anno["strand1"]+df_bedpe_anno["strand2"]+df_bedpe_anno["strand_gene1"]+df_bedpe_anno["strand_gene2"], np.nan)
    df_bedpe_anno["fusion"] = np.where((~df_bedpe_anno["fusion_orient"].isnull()) & \
    ((df_bedpe_anno["fusion_orient"] == "+++-") | \
     (df_bedpe_anno["fusion_orient"] == "+-++") | \
     (df_bedpe_anno["fusion_orient"] == "-+--") | \
     (df_bedpe_anno["fusion_orient"] == "---+") ) \
    , df_bedpe_anno["name_gene1"] + ":" + df_bedpe_anno["name_gene2"], ".")
    df_bedpe_anno["fusion"] = np.where((~df_bedpe_anno["fusion_orient"].isnull()) & \
    ((df_bedpe_anno["fusion_orient"] == "++-+") | \
     (df_bedpe_anno["fusion_orient"] == "+---") | \
     (df_bedpe_anno["fusion_orient"] == "-+++") | \
     (df_bedpe_anno["fusion_orient"] == "--+-") )\
    , df_bedpe_anno["name_gene2"] + ":" + df_bedpe_anno["name_gene1"], ".")
    return df_bedpe_anno

df_bedpe = pd.read_table(bedpe_file)
gencode_bed = BedTool(gencode_file)
gencode_chroms = gencode_bed.to_dataframe().iloc[:,0]
if gencode_chroms[2]  in df_bedpe.iloc[:, 0].tolist():
    df_out = annotate(df_bedpe, gencode_bed, max_distance = 50000)
    df_out.to_csv(fileOutAnno, sep = "\t", index = False)
    print ("\nAnnotation file\n\t", fileOutAnno)
else:
    print ("chromosome annotation differ between", gencode_file, "and", bedpe_file)
    sys.exit(0)
