import vcf
import os
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--vcf', dest='vcf', help='The VCF file to be filtered', required=True)
parser.add_argument('--geneList', dest='geneList', help='A list of accepted genes', required=False)
parser.add_argument('--tumor', dest='tumourName', help='Tumour sample name in VCF file', required=True)
parser.add_argument('--normal', dest='normalName', help='Normal sample name in VCF file', required=True)
parser.add_argument('--tVAFmin', dest='tVAFmin', help='The minimum required VAF in the tumor', required=False, type=float, default=0.1)
parser.add_argument('--nVAFmax', dest='nVAFmax', help='The maximum allowed VAF in the normal', required=False, type=float, default=0.01)
parser.add_argument('--tRDmin', dest='tRDmin', help='The minimum required read depth in the tumor', required=False, type=int, default=10)
parser.add_argument('--nRDmin', dest='nRDmin', help='The minimum required read depth in the normal', required=False, type=int, default=10)
parser.add_argument('--tADmin', dest='tADmin', help='The minimum required read depth supporting ALT in the tumour', required=False, type=int, default=2)
parser.add_argument('--nADmax', dest='nADmax', help='The maximum allowed read depth supporting ALT in the normal', required=False, type=int, default=1)
parser.add_argument('--passOnly', dest='passOnly', help='Remove variants with REJECT in the filter field', required=False, action="store_true", default=True)
parser.add_argument('--maxSOclass', dest='maxSOclass', help='The maximum allowed sequence ontology class (1-4)', choices=[1,2,3,4], type=int, required=False, default=2)
parser.add_argument('--printSOclasses', dest='printSOclasses', help='Print the  sequence ontology class members', required=False, action="store_true")
parser.add_argument('--debugLevel', dest='debugLevel', help='The desired debug output level', required=False, default=0, type=int)

args = parser.parse_args()


SOclasses  = [
#1 - HIGH
[
"transcript_ablation",
"splice_acceptor_variant",
"splice_donor_variant",
"stop_gained",
"frameshift_variant",
"stop_lost",
"initiator_codon_variant"
"start_lost",
"transcript_amplification",
"feature_truncation",
"feature_elongation"
],
#2 - MODERATE
[
"inframe_insertion",
"inframe_deletion",
"missense_variant",
"missense",
"protein_altering_variant"
],
#3 - LOW
[
"splice_region_variant",
"incomplete_terminal_codon_variant",
"stop_retained_variant",
"synonymous_variant",
"coding_sequence_variant",
],
#4 - MODIFIER
[
"mature_miRNA_variant",
"5_prime_UTR_variant",
"3_prime_UTR_variant",
"non_coding_transcript_exon_variant",
"nc_transcript_variant",
"intron_variant",
"NMD_transcript_variant",
"non_coding_transcript_variant",
"upstream_gene_variant",
"downstream_gene_variant",
"TFBS_ablation",
"TFBS_amplification",
"TF_binding_site_variant",
"regulatory_region_ablation",
"regulatory_region_amplification",
"regulatory_region_variant",
"intergenic_variant"
]]


def getCallDepthInfo(record, tumourName, normalName):
    callInfo = dict()
    callInfo['VAF']= dict()
    callInfo['DP']= dict()
    callInfo['DA']= dict()
    for sample in rec.samples:
        
        try:
            DA = float(sample['DA'])
            DP = float(sample['DP'])
        except ValueError:
            print("Failed to parse read depth values to float: DA: %s, DP: %s" % (sample['DA'],sample['DP']))
            exit(1007)
        
        try:
            VAF = round(DA/DP, 4)
        except ZeroDivisionError:
            if args.debugLevel > 3:
                print("Zero division error when calculating VAF. VAF set to 0: DA: %s, DP: %s" % (sample['DA'],sample['DP']))
            VAF=0
        
        if sample.sample == tumourName:            
            callInfo['VAF']['Tumour'] = VAF
            callInfo['DP']['Tumour'] = sample['DP']
            callInfo['DA']['Tumour'] = sample['DA']
        elif sample.sample == normalName:
            callInfo['VAF']['Normal'] = VAF
            callInfo['DP']['Normal'] = sample['DP']
            callInfo['DA']['Normal'] = sample['DA']
        else:
            print("Name on sample record does not match tumour or normal name: %s" % sample.sample)
            exit(200)
    return callInfo

def loadGeneList(fileLoc):
    geneList= []
    try:
        with open(fileLoc, 'rb') as f:
            for g in f:
                geneList.append(g.strip())
    except:
        print("Died while reading gene list from file")
        exit(2001)

    return geneList

##Read in geneList
if args.geneList:
    if ".txt" in args.geneList or ".tsv" in args.geneList:
        if os.path.isfile(args.geneList): 
            geneList = set(loadGeneList(args.geneList))
    else:
        geneList = set(args.geneList.split(","))
    
    if args.debugLevel > 1:
        print("Using gene filter list:")
        print(geneList)
else:
    geneList = False

#Populate SO terms
SOterms = []
if(args.maxSOclass>len(SOclasses)+1):
    print("Invalid maximum SO class. Maximum allowed value is 1 to %s" % str(len(SOclasses)))
    exit()

for i in range(0, args.maxSOclass, 1):
    SOterms = SOterms + SOclasses[i]
SOterms = set(SOterms)

if args.debugLevel > 0:
    print("Limiting consequences to:")
    print(SOterms)

vcfCompressed = False
if ".gz" in args.vcf or ".bcf" in args.vcf:
    vcfCompressed = True
    

if args.debugLevel > 0:   
    print ("Showing Degbug at level %s" % args.debugLevel)

VCFreader = vcf.Reader(filename=args.vcf, compressed=vcfCompressed)
nPass=0
nFail=0
for rec in VCFreader:
    if  args.passOnly and ("REJECT" in rec.FILTER):
        if args.debugLevel==5:
            print("Skipping REJECT variant")
        nFail+=1
        continue
    
    filterPass = 0
    nFilter = 8
    filterByteVal=1
    filterByteFinal = (2 ** nFilter) - 1

    callInfo = getCallDepthInfo(rec, args.tumourName, args.normalName)
    
    #1
    if callInfo['VAF']['Tumour'] >= args.tVAFmin:
        filterPass = filterPass | filterByteVal

    filterByteVal = filterByteVal << 1

    #2
    if callInfo['VAF']['Normal'] <= args.nVAFmax:
        filterPass = filterPass | filterByteVal
        
    filterByteVal = filterByteVal << 1    

    #4
    if callInfo['DP']['Tumour'] >= args.tRDmin:
        filterPass = filterPass | filterByteVal
           
    filterByteVal = filterByteVal << 1

    #8
    if callInfo['DP']['Normal'] >= args.nRDmin:
        filterPass = filterPass | filterByteVal
        
    filterByteVal = filterByteVal << 1

    #16
    if callInfo['DA']['Tumour'] >= args.tADmin:
        filterPass = filterPass | filterByteVal    
        
    filterByteVal = filterByteVal << 1

    #32
    if callInfo['DA']['Normal'] <= args.nADmax:
        filterPass = filterPass | filterByteVal    
        
    filterByteVal = filterByteVal << 1

    #64
    try:
        if  SOterms.intersection(set(rec.INFO['Ensembl_so_term'])) != set():
            filterPass = filterPass | filterByteVal        
    except KeyError:        
        filterPass = filterPass | filterByteVal  #### Remove this if adding flag to skip variants with no Ensembl_so_term 
        if args.debugLevel > 2:
            print ("Record missing Ensembl_so_term field")

    filterByteVal = filterByteVal << 1
    
    #128
    if geneList:
        if (geneList.intersection(set(rec.INFO['gene'])) != set() or geneList==set()):
            filterPass = filterPass | filterByteVal            
            print("Passed: Gene List")
            print(filterPass)     
        else:
            print("failed: Gene List")
    else: 
        filterPass = filterPass | filterByteVal
        
    output = dict()
    dynamicfieldlist = ["gene", "Ensembl_so_term", "HGVS_protein_change"]
    filterList=["UM75","BL.1","BL.2","BL.3","BL.4", "ExACFilter","BLmpu"]

    if filterPass == filterByteFinal:
        for f in dynamicfieldlist:
            try:
                output[f] = rec.INFO[f]
            except KeyError:
                output[f] = "-"
        
        Filters=';'.join(rec.FILTER)
        for f in filterList:
            try:
                if rec.INFO[f] == 1:
                    Filters = Filters  + ";" + f
            except KeyError:
                pass                
        
        print('\t'.join(
                [   
                    args.tumourName,
                    rec.CHROM, 
                    str(rec.POS),
                    str(rec.REF),
                    str(rec.ALT).replace("[",""). replace("]", ",").strip(","), 
                    str(rec.ID),
                    ','.join(output['gene']),
                    Filters.strip(";"),
                    ','.join(output['Ensembl_so_term']),
                    ','.join(map(str,[i for i, x in enumerate([','.join(output['Ensembl_so_term']) in c for c in SOclasses]) if x])),
                    ','.join(output['HGVS_protein_change']),
                    str(callInfo['VAF']['Normal']),
                    str(callInfo['VAF']['Tumour']),
                    str(callInfo['DP']['Normal']),
                    str(callInfo['DP']['Tumour'])
                ])
             )
        nPass+=1
    else:        
        if args.debugLevel==5:        
            print('\t'.join([rec.CHROM, str(rec.POS)]))
            print ("%s/%s" % (filterPass,filterByteFinal))
            if ((filterByteFinal-filterPass) & 1): print("-TumourVAF") 
            if ((filterByteFinal-filterPass) & 2): print("-NormalVAF") 
            if ((filterByteFinal-filterPass) & 4): print("-TumourDepth") 
            if ((filterByteFinal-filterPass) & 8): print("-NormalDepth") 
            if ((filterByteFinal-filterPass) & 16): print("-TumourAltReadDepth")
            if ((filterByteFinal-filterPass) & 32): print("-NormalAltReadDepth") 
            if ((filterByteFinal-filterPass) & 64): print("-SOTerm") 
            if ((filterByteFinal-filterPass) & 128): print("-GeneList") 
        #if ','.join(rec.INFO['gene'])=='EGFR':
        #    print("tVAFmin:{0}, nVAFmax:{1}, tRDmin:{2}, nRDmin:{3}, SOterms:{4}, geneList:{5},score:{6}".format(*list(map(str, [filterPass & 1, filterPass & 2, filterPass & 4, filterPass & 8, filterPass & 16, filterPass & 32, filterPass]))))
        #    print("tVAF:{0}, nVAF:{1}, nRD:{2}, tRD:{3}".format(*list(map(str, [callInfo['VAF']['Tumour'],callInfo['VAF']['Normal'],callInfo['DP']['Tumour'],callInfo['DP']['Normal']  ]))))
        nFail+=1

        

    
if args.debugLevel > 0:        
    print("Pass:"+str(nPass))
    print("Fail:"+str(nFail))


