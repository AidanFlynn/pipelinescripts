VAFfile=$1
annoFile=$2 
RDfile=$3
IFS=', ' read -r -a samples <<< "$4"

join -t$'\t' -j 1 <(cut -f1,2,5 $annoFile | sed 's/\t/$/' | sort ) <(tail -n+2 $VAFfile | sed 's/\t/$/' | sort) | tr '$' $'\t' | sort -k1,2 -V | cat <(head -n1 $VAFfile | sed 's/Pos/Pos\tGene/')  - > vaf.tmp

minReadDepth=50
minReadDepthRescueList=25


Rscript ~/miscScripts/VAFtoCanopy.R vaf.tmp $RDfile $minReadDepth ~/RescueList.txt $minReadDepthRescueList

rm vaf.tmp 


for sample in ${samples[@]}; do
  mv /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/${sample}/analysis/sequenza/${sample}T_mutations.txt /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/${sample}/analysis/sequenza/${sample}T_mutations.bak$(date +"%M%S")
  mv ${sample}T_mutations.txt /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/${sample}/analysis/sequenza/${sample}T_mutations.txt
done
mv selected_mutations.txt /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/${samples[0]}/analysis/canopy/
mv canopy_input_debug.txt /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/${samples[0]}/analysis/canopy/



