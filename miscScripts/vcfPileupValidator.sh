##ARGS: NormalSample TumourSample1 TumourSample2 TumourSampleN

module load bcftools/1.8
module load samtools/1.8

Samples=($@)
Normal=${Samples[0]}
Tumours=${Samples[@]:1}

REF_PATH="/home/databases/sanger/PanCancer/hs37d5/genome.fa"

baseDir="/home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion"

posfile="$baseDir/$Normal/analysis/mutect/$(sed 's/ /_/g' <<< ${Tumours[@]})_positions.tsv"

vcfmerge_cmd="bcftools merge --force-samples "
for Tumour in ${Tumours[@]};
do
vcfmerge_cmd="$vcfmerge_cmd $baseDir/$Tumour/analysis/mutect/mutect_${Tumour}T.oncotator.pass.bcf"
done
vcfmerge_cmd="$vcfmerge_cmd | bcftools view -i \"INFO/UM75=0 && INFO/BL.2=0 && INFO/ExACFilter=0\" | bcftools query -f \"%CHROM\t%POS\t%REF\t%ALT\t%INFO/gene\t%INFO/Ensembl_so_term\t%INFO/protein_change\n\"" 
echo "CHROM POS REF ALT Gene Consequence AAChange" | tr ' ' $'\t' > $posfile
eval $vcfmerge_cmd  >> $posfile

mpileup_cmd="samtools mpileup -BQ0 -d10000000 --positions <(cut -f1,2 $posfile | tail -n+2) --fasta-ref /home/databases/sanger/PanCancer/hs37d5/genome.fa"
mpileup_cmd="$mpileup_cmd $baseDir/$Normal/${Normal}N_rmdup.cram"
for Tumour in ${Tumours[@]};
do
mpileup_cmd="$mpileup_cmd $baseDir/$Tumour/${Tumour}T_rmdup.cram"
done

mpileupfile="$baseDir/$Normal/analysis/mutect/$(sed 's/ /_/g' <<< ${Tumours[@]})_mpileup.txt"

eval $mpileup_cmd > $mpileupfile

collapse_rex='s/^([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t/\1%\2%\3%\4\t/'

join -t $'\t' \
<(~/miscScripts/mpileup2VAF.py \
--pileup $mpileupfile \
--pileupSamples ${Normal}N $(sed -r 's/ |$/T /g' <<< ${Tumours[@]}) \
--mutectCalls <(tail -n+2 $posfile) \
--outputValue All | sed -r $collapse_rex | (read h; echo "$h"; sort)) \
<(sed -r $collapse_rex $posfile | (read h; echo "$h"; sort)) | sed 's/%/\t/g'

