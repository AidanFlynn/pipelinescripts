library(ggplot2)
library(reshape2)
library(gridExtra)

#' Convert Paired Parent/Child list into plotting levels
#' @param CanopyTree The canopy solution for plotting,  obtained from canopy.output()
#' @return A list of nodes at each level of the hierarchy 
getNodeList <- function(CanopyTree)
{
  TreeEdges <- CanopyTree$edge
  TreeEdges <- TreeEdges[TreeEdges[,2]!=1,]
  
  TopNode <- TreeEdges[!(TreeEdges[,1] %in% TreeEdges[,2]),1][1]
  TerminalNodes <- TreeEdges[!(TreeEdges[,2] %in% TreeEdges[,1]),2]
  Nodes <- list()
  Nodes[1] <- TopNode
  
  i=2 # start from second level
  repeat {
    Nodes[[i]] <- unlist(TreeEdges[(TreeEdges[,1] %in% Nodes[[i-1]]),2])
    Nodes[[i]] <- Nodes[[i]][!(Nodes[[i]] %in% TerminalNodes)]
    if(all(Nodes[[i]] %in% TerminalNodes)) { break }
    i=i+1
  }
  Nodes[[i]] <- TerminalNodes
  return(Nodes)
}

#' Create a list of features that belong to each Parent/Child node relationship
#' @param CanopyTree The canopy solution for plotting,  obtained from canopy.output()
#' @param Mutations A list of mutations to be annotated, must match names in selected_mutations file, providing TRUE/FALSE lists all or no mutations respectively
#' @param CNA A list of CNA events to be annotated, must match names in selected_segments file, providing TRUE/FALSE lists all or no mutations respectively
#' @param uniqueSep The separator used when using make.unique() to prevent duplicate names, only require when KeepUnique=FALSE
#' @param KeepUnique If TRUE, an attempt will be made to strip up to two characters [Aa-Zz0-9] after the character indicated in uniqueSep from gene names
#' @param MutationType A dataframe of mutations name and annotation value which will be used to color the mutatation annotation text, when used you should manually provide sufficient AnnotationColors when calling ggCanopy()   
#' @return A dataframe listing the Features with the parent child relationship to which they belong and the type of event
getFeaturesPerNode <- function(CanopyTree, Mutations = NULL, CNA=T, KeepUnique=T, uniqueSep="_", MutationType=NULL)
{
  sna <- data.frame(CanopyTree$sna)[,-1]
  sna$Feature <- rownames(sna)
  
  #remove incrementals from duplicate gene names
  if(!KeepUnique)
  {
    sna$Feature <- gsub(x=sna$Feature, pattern = paste("(.*)","[A-Za-z0-9]{1,2}", sep = uniqueSep),replacement = "\\1")
  }
  
  sna<- unique(sna)
  colnames(sna) <- c("ParentNodeID","ChildNodeID","Feature")
  
  #Filter for only desired mutations
  if(!is.null(Mutations))
  {
    sna <- sna[sna$Feature %in% Mutations,]
  }
  
  #Annotate with mutation Annotations if supplied
  if(!is.null(MutationType))
  {
    sna <- merge(sna, MutationType, by="Feature")
  }
  else
  {
    sna$Type="Mutation"
  }
  
  cna <- data.frame(CanopyTree$cna)[,-1] 
  cna$Feature <- rownames(CanopyTree$cna)
  
  #split off event type and annotate
  cna$Feature <- gsub(x = cna$Feature,pattern = "(.+)_([^_]+)$", replacement = "\\1?\\2", perl = T)
  temp <- do.call(rbind, strsplit(cna$Feature, split = "?", fixed = T))
  cna <- data.frame(ParentNodeID=cna$cna.st.node, ChildNodeID=cna$cna.ed.node, Feature=temp[,1], Type=temp[,2])
  
  #Filter desired CNAs for annotation
  if(class(CNA)=="logical")
  {
    if (CNA==F)
    {
      cna <- cna[NULL,]
    }
  } else 
  {
    cna <- cna[cna$Feature %in% CNA,]
  }
  
  NodeFeatures <- rbind(sna,cna)
  NodeFeatures$Type <- factor(as.character(NodeFeatures$Type), levels = c(unique(sort(as.character(sna$Type))), "del", "dup", "LOH"))
  #levels(NodeFeatures$Type) <- c(unique(sort(as.character(sna$Type))), "Deletion", "Duplication", "LOH")
  
  if(nrow(NodeFeatures)==0) { warning("No features found matching Features of interest list")}
  return(NodeFeatures)
}

#' Process Nodes and Annotations to produce plotting coordinates 
#' @param CanopyTree The canopy solution for plotting,  obtained from canopy.output()
#' @param NodeFeatures The ouput from getFeaturesPerNode()
#' @param Gap The distance between each terminal node along the x axis
#' @return A list of dataframes and objects containing the data required for plotting
getGraphObjects <- function (CanopyTree, NodeFeatures, Gap=2) {
  Nodes <- getNodeList(CanopyTree)
  
  Clonality <- CanopyTree$P

  TotalWidth=length(Nodes[[length(Nodes)]])*Gap
  
  graphObject <- list()
  
  Connectors <- data.frame(ParentNodeID=vector(), 
                           ChildNodeID=vector(), 
                           xStart=vector(), 
                           xEnd=vector(), 
                           yStart=vector(), 
                           yEnd=vector(), 
                           axis=vector())
  
  NodePoints <- data.frame(NodeID=vector(), 
                           xPosition=vector(), 
                           yPosition=vector(), 
                           Clone=vector(), 
                           Scale=vector())
  
  for (i in length(Nodes):1)
  {
    if(i==length(Nodes)) #terminal nodes
    {
      xPos <- seq(1,TotalWidth,Gap)
      NodePoints <- rbind(NodePoints, 
                          data.frame(NodeID=Nodes[[i]], 
                                     xPosition=xPos, 
                                     yPosition=i, 
                                     Clone=paste("Clone", Nodes[[i]]-1), 
                                     Scale=rowSums(Clonality[-1,])))
    } else #ancestral nodes
    {
      for(N in Nodes[[i]]) 
      {
        cNodes <- output.tree$edge[output.tree$edge[,1]==N,2] #Get Child nodes
        cNodeMin <- min(NodePoints$xPosition[NodePoints$NodeID %in% cNodes]) #Minimum child node X position
        cNodeMax <- max(NodePoints$xPosition[NodePoints$NodeID %in% cNodes]) #Maximum child node X position
        xPos <- ifelse(cNodeMin==cNodeMax, cNodeMax, cNodeMax-((cNodeMax-cNodeMin)/2)) #set node position between max/min of children
        
        #Generate coordinates for node and add it to the list
        NodePoints <- rbind(NodePoints, data.frame(NodeID=N, xPosition=xPos, yPosition=i, Clone="Ancestor", Scale=0.25))
   
        #Generate coordinates for lines between parent and child nodes and add them to the list
        Connectors <- rbind(Connectors, 
                            #horizonatal lines
                            data.frame(ParentNodeID=N, 
                                       ChildNodeID=cNodes, 
                                       xStart=xPos, 
                                       xEnd=NodePoints$xPosition[NodePoints$NodeID %in% cNodes], 
                                       yStart=i, 
                                       yEnd=i, 
                                       axis="h"),
                            #vertical lines
                            data.frame(ParentNodeID=N, 
                                       ChildNodeID=cNodes, 
                                       xStart=NodePoints$xPosition[NodePoints$NodeID %in% cNodes], 
                                       xEnd=NodePoints$xPosition[NodePoints$NodeID %in% cNodes], 
                                       yStart=i, 
                                       yEnd=NodePoints$yPosition[NodePoints$NodeID %in% cNodes], 
                                       axis="v"))
          
      }
    }
  }

  #remove residual row names
  rownames(NodePoints) <- 1:nrow(NodePoints)
  
  #flip yPositions top to bottom because graphing coordinate is opposite of hierarchy level
  NodePoints$yPosition <- (max(NodePoints$yPosition)+1)-NodePoints$yPosition
  Connectors$yStart <- (max(NodePoints$yPosition)+1)-Connectors$yStart
  Connectors$yEnd <- (max(NodePoints$yPosition)+1)-Connectors$yEnd
  
  #Add cooordinates for Annotations based on connectors
  Annotations <- merge(Connectors[Connectors$axis=="v",], NodeFeatures, by=c("ParentNodeID","ChildNodeID"))
  Annotations$yStart <- (Annotations$yStart)-((Annotations$yStart-Annotations$yEnd)/2)
  Annotations$xStart <- Annotations$xStart
  Annotations <- Annotations[,-c(4,6,7)]
  
  NodePoints$Clone <- factor(as.character(NodePoints$Clone), levels=sort(unique(as.character(NodePoints$Clone))))
  graphObject$NodePoints <- NodePoints
  graphObject$Connectors <- Connectors
  graphObject$Annotations <- Annotations
  graphObject$Gap=Gap
  graphObject$nTerminalNodes=length(Nodes[[length(Nodes)]])
  
  #Rename and get sizes of clones 
  Clonality <- Clonality[c(2:nrow(Clonality),1),]
  Clonality <- melt(Clonality)
  colnames(Clonality) <- c("Clone","Sample","Scale")
  Clonality <- cbind(Clonality, xPosition=rep(seq(1,TotalWidth+Gap,Gap)), yPosition=as.numeric(Clonality$Sample))
  levels(Clonality$Clone) <- gsub("clone1","Normal", levels(Clonality$Clone))
  levels(Clonality$Clone)[1:(length(levels(Clonality$Clone))-1)] <- paste("Clone", 1:(length(levels(Clonality$Clone))-1))
  
  #Normalise factors for colouring
  Clonality$Clone <- factor(as.character(Clonality$Clone), levels=c("Ancestor", levels(Clonality$Clone)))
  NodePoints$Clone <- factor(as.character(NodePoints$Clone), levels=c(levels(NodePoints$Clone),"Normal"))
  
  graphObject$Clonality <- Clonality
  
  return(graphObject)
}

#' Call ggPlot to produce plot objects 
#' @param graphObjects The output from getGraphObjects()
#' @param AnnotationOffsetV The vertical distance between annotation text items
#' @param AnnotationOffsetH The horizontal offset of annotation text items
#' @param CloneColors A list of colors to use for the clones
#' @param AnnotationColors A list of colors to use for the Annotations, should match the number of levels in getGraphObjects(...)$Annotations$Type
#' @param annotSize Size of the annotation text
#' @param LeftPadding Margin padding to the left of each plot to allow for longer sample names
#' @return ggPlot grobs wrapped by grid.arrange()  
ggCanopy <- function(graphObjects, AnnotationOffsetV= 0.15, AnnotationOffsetH=0.1, annotSize=4, AnnotationColors=NULL, CloneColors=NULL, LeftPadding=0.5){

  if(nrow(graphObjects$Annotations)>0)  {
    graphObjects$Annotations <- adjustYOffset(graphObjects$Annotations, vOffset = AnnotationOffsetV)
  }
  
  #default clone colors
  if(is.null(CloneColors))
  {
    CloneColors <- c("black","tomato2", "aquamarine3","deepskyblue4","sienna2","mediumorchid2","lightsteelblue3", "peachpuff", "slategrey", "navy")
  }
  
  #default Annotation colors
  if(is.null(AnnotationColors)) {  AnnotationColors <- c("black","blue","red","purple")}
  
  #Tree plot
  p <- ggplot() 
  p <- p + geom_segment(data = graphObjects$Connectors,mapping = aes(x=xStart, y=yStart, xend=xEnd, yend=yEnd)) 
  p <- p + geom_point(data = graphObjects$NodePoints, mapping = aes(x=xPosition, y=yPosition,fill=Clone,size=Scale), color="black", stroke=1.5, shape=21) 
  p <- p + geom_text(data = graphObjects$Annotations, mapping = aes(x=xStart,y=yStart,label=Feature, colour=Type),nudge_x = AnnotationOffsetH, hjust=0, size=annotSize) 
  p <- p + scale_fill_manual(values = CloneColors)
  p <- p + scale_color_manual(values = AnnotationColors)
  p <- p + scale_size_area(max_size = 10) 
  p <- p + guides(size=F, fill=F, color=F) 
  p <- p + theme_void()
  p <- p + xlim(-LeftPadding,(graphObjects$Gap*graphObjects$nTerminalNodes)+graphObjects$Gap)
  
  #Clone contribution plot
  p2 <- ggplot(graphObjects$Clonality) 
  p2 <- p2 + geom_point( aes(x=xPosition, y=yPosition,fill=Clone,size=Scale), color="black", stroke=1.5, shape=21)
  p2 <- p2 + scale_fill_manual(values = CloneColors[-1])
  p2 <- p2 + scale_size_area(max_size = 10) 
  p2 <- p2 + xlim(-LeftPadding,(GO$Gap*GO$nTerminalNodes)+graphObjects$Gap)
  p2 <- p2 + guides(size=F, fill=F)
  p2 <- p2 + theme_void()
  p2 <- p2 + ylim(0,length(levels(graphObjects$Clonality$Sample))+1)
  p2 <- p2 + geom_text(mapping = aes(x=0,y=yPosition,label=Sample))
  p2 <- p2 + geom_text(mapping = aes(x=xPosition,y=0,label=Clone))

  returnPlots <- grid.arrange(p, p2,  heights=c(5,2))
  return(returnPlots)
}



#' Private helper-function to adjust annotation Y-positions based on desired offset
#' @param Annotations A list of annotations and pre-offset coordinates
#' @return A list of annotations with offset coordinates
adjustYOffset <- function (Annotations, vOffset=0.1)
{
  returnTable <- Annotations[NULL,]
  uniqpairs <- unique(Annotations[,c(1,2)])
  for (i in 1:nrow(uniqpairs))
  {
    subTable <- Annotations[Annotations$ParentNodeID==uniqpairs$ParentNodeID[i] & Annotations$ChildNodeID==uniqpairs$ChildNodeID[i],]
    nFeature<-length(subTable$yStart)
    subTable$yStart <- subTable$yStart + (seq(vOffset,nFeature*vOffset,vOffset) - mean(seq(vOffset,nFeature*vOffset,vOffset)))
    returnTable <- rbind(returnTable, subTable)
  }
  return(returnTable)
}



