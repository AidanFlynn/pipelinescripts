#! /usr/bin/awk -f
{
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/logs/%s \n", $2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/bamqc  \n", $1,$1;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/bamqc  \n", $2,$2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/teqc \n", $1,$1;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/teqc \n", $2,$2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/fastqc \n", $1,$1;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/fastqc \n", $2,$2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/seqstat \n", $1,$1; 
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/seqstat  \n", $2,$2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/contest  \n", $2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/sequenza/seqz  \n", $2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/mutect  \n", $2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/freebayes  \n", $2;
printf "mkdir -p /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/platypus \n", $2;
printf "pype pipelines --log /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/logs/%s exome_complete \\\n", $2;
printf "--normal_name %sN \\\n", $1;\
printf "--tumor_name %sT \\\n", $2;\
printf "--tumor_bam /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/%sT.bam \\\n", $2,$2;
printf "--normal_bam /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/%sN.bam \\\n", $1,$1;
printf "--fastqc_tumor_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/fastqc \\\n", $2,$2;
printf "--fastqc_normal_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/fastqc \\\n", $1,$1;
printf "--bamqc_tumor_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/bamqc \\\n", $2,$2;
printf "--bamqc_normal_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/bamqc \\\n", $1,$1;
printf "--seqstat_tumor_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/seqstat \\\n", $2,$2;
printf "--seqstat_normal_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/seqstat \\\n", $1,$1;
printf "--teqc_tumor_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sT/teqc \\\n", $2,$2;
printf "--teqc_normal_outdir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/%sN/teqc \\\n", $1,$1;
printf "--contest_normal_table /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/contest/%sN.contest.table.txt \\\n", $2,$1;
printf "--contest_tumor_table /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/contest/%sT.contest.table.txt \\\n", $2,$2;
printf "--contest_out /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/qc/contest/%s.contest.out.txt \\\n", $2,$2;
printf "--normal_fq1 %s  \\\n", $3;
printf "--normal_fq2 %s  \\\n", $4;
printf "--tumor_fq1 %s  \\\n", $5;
printf "--tumor_fq2 %s  \\\n", $6;
printf "--sequenza_out_dir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/sequenza  \\\n", $2;
printf "--seqz_out_prefix /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/sequenza/seqz/%s.seqz  \\\n", $2,$2;
printf "--mutect_out_dir /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/mutect  \\\n", $2;
printf "--oncotator_out_vcf /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/mutect/mutect_%sT.oncotator.vcf  \\\n", $2,$2;
printf "--freebayes_out_vcf /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/freebayes/freebayes_%s.vcf  \\\n", $2,$2;
printf "--platypus_out_vcf /home/projects/cu_10027/projects/gbm/data/data_processed/rigshospitalet/exome/singleregion/%s/analysis/platypus/platypus_%s.vcf  \\\n", $2,$2;
printf "--teqc_exome_target_name %s  \\\n", $7;
printf "--exome_target_file %s  \\\n", $8;
printf "--tmp_dir /home/projects/cu_10027/scratch \n\n";
}

