import re
import sys

badFields = [
    'dbNSFP_1000Gp1_AC',
    'dbNSFP_1000Gp1_AF',
    'dbNSFP_1000Gp1_AFR_AC',
    'dbNSFP_1000Gp1_AFR_AF',
    'dbNSFP_1000Gp1_AMR_AC',
    'dbNSFP_1000Gp1_AMR_AF',
    'dbNSFP_1000Gp1_ASN_AC',
    'dbNSFP_1000Gp1_ASN_AF',
    'dbNSFP_1000Gp1_EUR_AC',
    'dbNSFP_1000Gp1_EUR_AF',
    'dbNSFP_CADD_phred',
    'dbNSFP_CADD_raw',
    'dbNSFP_CADD_raw_rankscore',
    'dbNSFP_ESP6500_AA_AF',
    'dbNSFP_ESP6500_EA_AF',
    'dbNSFP_FATHMM_rankscore',
    'dbNSFP_FATHMM_score',
    'dbNSFP_GERP++_NR',
    'dbNSFP_GERP++_RS',
    'dbNSFP_GERP++_RS_rankscore',
    'dbNSFP_LRT_Omega',
    'dbNSFP_LRT_converted_rankscore',
    'dbNSFP_LRT_score',
    'dbNSFP_LR_rankscore',
    'dbNSFP_LR_score',
    'dbNSFP_MutationAssessor_rankscore',
    'dbNSFP_MutationAssessor_score',
    'dbNSFP_MutationTaster_converted_rankscore',
    'dbNSFP_MutationTaster_score',
    'dbNSFP_Polyphen2_HDIV_rankscore',
    'dbNSFP_Polyphen2_HDIV_score',
    'dbNSFP_Polyphen2_HVAR_rankscore',
    'dbNSFP_Polyphen2_HVAR_score',
    'dbNSFP_RadialSVM_rankscore',
    'dbNSFP_RadialSVM_score',
    'dbNSFP_Reliability_index',
    'dbNSFP_SIFT_converted_rankscore',
    'dbNSFP_SIFT_score',
    'dbNSFP_SLR_test_statistic',
    'dbNSFP_SiPhy_29way_logOdds',
    'dbNSFP_SiPhy_29way_logOdds_rankscore',
    'dbNSFP_Uniprot_aapos',
    'dbNSFP_aapos',
    'dbNSFP_codonpos',
    'dbNSFP_fold-degenerate',
    'dbNSFP_hg18_pos(1-coor)',
    'dbNSFP_phastCons100way_vertebrate',
    'dbNSFP_phastCons100way_vertebrate_rankscore',
    'dbNSFP_phastCons46way_placental',
    'dbNSFP_phastCons46way_placental_rankscore',
    'dbNSFP_phastCons46way_primate',
    'dbNSFP_phastCons46way_primate_rankscore',
    'dbNSFP_phyloP100way_vertebrate',
    'dbNSFP_phyloP100way_vertebrate_rankscore',
    'dbNSFP_phyloP46way_placental',
    'dbNSFP_phyloP46way_placental_rankscore',
    'dbNSFP_phyloP46way_primate',
    'dbNSFP_phyloP46way_primate_rankscore',
    '1000gp3_AA',
    '1000gp3_AC',
    '1000gp3_AF',
    '1000gp3_AFR_AF',
    '1000gp3_AMR_AF',
    '1000gp3_AN',
    '1000gp3_CIEND',
    '1000gp3_CIPOS',
    '1000gp3_CS',
    '1000gp3_DP',
    '1000gp3_EAS_AF',
    '1000gp3_END',
    '1000gp3_EUR_AF',
    '1000gp3_IMPRECISE',
    '1000gp3_MC',
    '1000gp3_MEINFO',
    '1000gp3_MEND',
    '1000gp3_MLEN',
    '1000gp3_MSTART',
    '1000gp3_NS',
    '1000gp3_SAS_AF',
    '1000gp3_SVLEN',
    '1000gp3_SVTYPE',
    '1000gp3_TSD',
]

fhi = open(sys.argv[1],'r')
if sys.argv[2] != "-":
    fho = open(sys.argv[2],'w')

for line in fhi:
    line=line.rstrip("\r\n")
    if line.startswith('#'):
        for badField in badFields:
            if badField in line:
                line = line.replace("Float","String")
                line = line.replace("Integer", "String")       
                
        line = line.replace('##FORMAT=<ID=AD,Number=.,Type=Integer,Description="Allelic depths for the ref and alt alleles in the order listed">',
                            '##FORMAT=<ID=DR,Number=.,Type=Integer,Description="Allelic depths for the ref alleles">\n##FORMAT=<ID=DA,Number=.,Type=Integer,Description="Allelic depths for the alt allele">')
    else:
        
        try:
            CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT, samples = line.split("\t",9)
        except ValueError:
            print("Line Split Failed with line: %s" % line)
            exit(1)
        samples = samples.split("\t")
        sampleout = []
        NewFORMAT = FORMAT
        for sample in samples:
            data = dict(zip(FORMAT.split(':'), sample.split(':')))
            if '|' in data['AD']:
                DR, DA = data['AD'].split('|')
                data.pop('AD')
                data['DR'] = DR
                data['DA'] = DA
            NewFORMAT=':'.join(sorted(data.keys()))
            sampleout.append(':'.join([data[j] for j in sorted(data.keys())]))
        

        line = "\t".join([CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,NewFORMAT,'\t'.join(sampleout)])
    if sys.argv[2] != "-":
        fho.write(line+"\n")
    else:
        print(line)
    

