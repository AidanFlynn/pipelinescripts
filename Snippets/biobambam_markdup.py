import os
import shlex
import subprocess

from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    infile = argv['--bam']
    suffix  = argv['--suffix']
    outfile=infile.replace(".bam","%s.bam" % suffix)

    return({'bamout': outfile, 'bai': '%s.bai' % outfile})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Mark duplicates with biobambam'),
                                 add_help=False)


def biobambam_markdup_args(parser, subparsers, argv):
    parser.add_argument('--bam', dest='bam', 
                        help='BAM file mark duplicates', required=True)
    parser.add_argument('--suffix', dest='suffix',
                        help='Suffix to add before .bam extension to create outfile (i.e name.bam=name.suffix.bam). Default:_rmdup', default="_rmdup")
    parser.add_argument('--markonly', dest='markonly',
                        help='Mark duplicate reads rather than remove them',  action='store_true')
    parser.add_argument('--tmpdir', dest='tmp',
                        help='Temporary folder')
    return parser.parse_args(argv)


def biobambam_markdup(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = biobambam_markdup_args(add_parser(subparsers, module_name),
                                subparsers, argv)

    outfile=args.bam.replace(".bam","%s.bam" % args.suffix)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['biobambam2']))

    log.log.info('Preparing biobambam2 bammarkduplicates2  command line')
    
    rmDup = "1"
    if args.markonly:
        rmDup="0"
        
    
    random_str = generate_uid()
    markdup_cmd = ['bammarkduplicates2', 'O=%s' % outfile, 'index=1',
                     'indexfilename=%s.bai' % outfile, 'rmdup=%s' % rmDup,
                     'tmpfile=%s' % os.path.join(args.tmp, 'bammarkduplicates2_%s' % random_str),
                     'I=%s' % args.bam]
    
    log.log.info(' '.join(map(str, markdup_cmd)))
    markdup_cmd = shlex.split(' '.join(map(str, markdup_cmd)))
    log.log.info('Execute bam bammarkduplicates2 with python subprocess.Popen')
    markdup_proc = subprocess.Popen(markdup_cmd, stdout=subprocess.PIPE)
    out0 = markdup_proc.communicate()[0]
    log.log.info('Terminate bammarkduplicates2')
