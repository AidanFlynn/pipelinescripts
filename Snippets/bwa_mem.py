import os
import re
import gzip
import shlex
import subprocess
from pype.misc import generate_uid
from pype.binfmisc import read_fastq_tag, fastq_header_regex, make_readgroup
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 8, 'time': '50:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Align fastQ file with the '
                                       'BWA mem algorithm'),
                                 add_help=False)


def bwa_mem_args(parser, subparsers, argv):
    parser.add_argument('-s', dest='samplename',
                        help=('Sample Name'),
                        required=True)
    parser.add_argument('-h', dest='header',
                        help=('Header file containing the @RG groups, or '
                              'the comma separated header line'),
                        required=False, default="FromF1")
    parser.add_argument('-1', dest='f1',
                        help='First mate fastQ file',
                        required=True)
    parser.add_argument('-2', dest='f2',
                        help=('Second mate fastQ file. '
                              'Not required if single end'),
                        required=False)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/home/projects/cu_10027/scratch/')
    parser.add_argument('-o', '--out', dest='out',
                        help='Output name for the bam file',
                        default=None)
    parser.add_argument('-j', '--threads', dest='threads',
                        help='Number of parallel processes',
                        default="8")                        
    return parser.parse_args(argv)


def bwa_mem(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = bwa_mem_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['bwa']))
    module('add', program_string(profile.programs['biobambam2']))
    module('add', program_string(profile.programs['samtools_1']))

    genome = profile.files['genome_fa_gz']
    log.log.info('Use genome file %s' % genome)

    header = None

    log.log.info('Read the reads group tag (@RG):')
    if args.header == 'FromF1':
        try:
            header = make_readgroup(read_fastq_tag(args.f1, fastq_header_regex()), args.samplename)
            log.log.info('Generated RG tag as: %s' % header)
        except:
            log.log.error('Failed to read tag from Fastq')
            exit(101)
    elif args.header.startswith('@RG'):
        header = '\t'.join(args.header.rstrip().split(','))
    else:
        try:
            with open(args.header, 'rb') as head_file:
                for line in head_file:
                    if line.startswith('@RG'):
                        header = line.rstrip()
                        break
        except IOError as e:
            log.log.error('@RG argument %s not a file ' % args.header)
            raise e
    if not header:
        log.log.error('No read group available')
    if args.f2 is None:
        log.log.info('Prepare single end alignemnt')
        fastq_files = [args.f1]
    else:
        log.log.info('Prepare paired end alignemnt')
        fastq_files = [args.f1, args.f2]
    log.log.info('Prepare bwa mem command line')
    header = "\'%s\'" % header.replace('\t', '\\t')
    bwa_cmd = ['bwa', 'mem', '-t', args.threads, '-T', '0', '-M',
               '-R', header, genome] + fastq_files
    log.log.info(' '.join(map(str, bwa_cmd)))
    log.log.info('Prepare bam sorting command line')
    random_str = generate_uid()

    bamsort_cmd = ['samtools', 'sort', '-l', '1', '-@', '2', '-O', 'bam',
                   '-T', '%s' % os.path.join(args.tmp,
                                             'bamsort_%s' % random_str)]

    if args.out:
        # bamsort_cmd.append('O=%s' % args.out)
        bamsort_cmd = bamsort_cmd + ['-o', '%s' % args.out]

    log.log.info(' '.join(map(str, bamsort_cmd)))
    bwa_cmd = shlex.split(' '.join(map(str, bwa_cmd)))
    bamsort_cmd = shlex.split(' '.join(map(str, bamsort_cmd)))
    log.log.info('Execute bwa mem with python subprocess.Popen')
    bwa_proc = subprocess.Popen(bwa_cmd, stdout=subprocess.PIPE)
    log.log.info('Execute bam sorting with python subprocess.Popen')
    bamsort_proc = subprocess.Popen(bamsort_cmd, stdin=bwa_proc.stdout,
                                    stdout=subprocess.PIPE)
    bwa_proc.stdout.close()
    out0 = bamsort_proc.communicate()[0]
    log.log.info('Terminate bam sorting')

    if args.out:
        log.log.info('Creating BAM indexes.')
        samtools_cmd = ['samtools', 'index', args.out]
        samtools_proc = subprocess.Popen(samtools_cmd)
        out1 = samtools_proc.communicate()[0]
