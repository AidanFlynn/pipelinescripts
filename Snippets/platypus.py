import os
import shlex
import subprocess

from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 8, 'time': '24:00:00'})


def results(argv):
    outfile = argv['--vcfout']
    return({'vcf': outfile})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Call INDELS with Platypus'),
                                 add_help=False)


def platypus_args(parser, subparsers, argv):
    parser.add_argument('--vcfout', dest='vcfout', 
                        help='Variant call output file', required=True)
    parser.add_argument('--bamA', dest='bamA', 
                        help='Input BAM file', required=True)                        
    parser.add_argument('--bamB', dest='bamB', 
                        help='Optional second input BAM file', required=False)                                                
    parser.add_argument('--refFile', dest='ref',
                        help='Reference FASTA file', required=False)
    parser.add_argument('--nCPU', dest='nCPU',
                        help='Number of CPUs to use', default=1, type=int)
    parser.add_argument('--mergeClusteredVariants', dest='MCV',
                        help='If set to 1, variant-containing windows which are close together will be merged, resulting in slower, more accurate variant calls in diverse regions', default="1")
    parser.add_argument('--minFlank', dest='minFlank',
                        help='Ignore base-changes closer than minFlank bases to the end of reads. Also, merge SNPs within this distance into MNPs or complex replacements', default="3")
    
    return parser.parse_args(argv)

def platypus(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = platypus_args(add_parser(subparsers, module_name),
                                subparsers, argv)

    if not args.ref:
        refFile = profile.files['genome_fa']
    else:
        refFile = args.ref

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['htslib']))
    module('add', program_string(profile.programs['platypus']))

    log.log.info('Preparing Platypus command line')
    
    
    if args.bamB:
        inputbam = "%s,%s" % (args.bamA, args.bamB)
    else:
        inputbam = args.bamA

    platypus_cmd = ['Platypus.py', 'callVariants',
                     '--output=%s' %  args.vcfout, 
                     '--bamFiles=%s' %  inputbam,
                     '--refFile=%s' %  refFile, 
                     '--nCPU=%s' %  args.nCPU,
                     '--logFileName=%s.platypus_log' %  args.vcfout, 
                     '--minFlank=%s' %  args.minFlank,
                     '--mergeClusteredVariants=%s' %  args.MCV]
    
    log.log.info(' '.join(map(str, platypus_cmd)))
    platypus_cmd = shlex.split(' '.join(map(str, platypus_cmd)))
    log.log.info('Execute bam platypus with python subprocess.Popen')
    platypus_proc = subprocess.Popen(platypus_cmd, stdout=subprocess.PIPE)
    out0 = platypus_proc.communicate()[0]
    log.log.info(out0)
    log.log.info('Terminate platypus')
