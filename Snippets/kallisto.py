
import os
import shlex
import subprocess
import re
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '01:00:00'})


def results(argv):
    try:
        outdir = argv['--outdir']
    except KeyError:
        outdir = argv['-o']
    finishFile = os.path.join(outdir,"kallisto.finished")
    return {'kallistofinished':finishFile}

def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Perform RNA-Seq feature counting with Kallisto'),
                                 add_help=False)

                                 
def kallisto_args(parser, subparsers, argv):
    parser.add_argument('--fastq1', dest='fastq1',
                        help=('Read 1 fastq file'),
                        required=True)
    parser.add_argument('--fastq2', dest='fastq2',
                        help=('Read 2 fastq file'),
                        required=False, default=None)  
    parser.add_argument('--outdir', dest='outdir',
                        help='Create all output files in the specified output directory',
                        required=True)
    parser.add_argument('--index', dest='index',
                        help=('Filename for the kallisto index to be used for quantification. See /home/projects/cu_10027/data/genomes/kallisto'),
                        required=True)                        
    parser.add_argument('--nobias', action='store_true',
                        help='Do NOT perform sequence based bias correction',
                        required=False)
    parser.add_argument('--bootstrap-samples', dest='bootstrapsamples',
                        help='Number of bootstrap samples',
                        required=False, default="0")
    parser.add_argument('--seed', dest='seed',
                        help='Seed for the bootstrap sampling',
                        required=False, default='42')
    parser.add_argument('--fusion', action='store_true',
                        help=('Output the files required by Pizzly fusion detection'),
                        required=False)                        
    parser.add_argument('--stranded', dest='stranded',
                        help=('[fwd/rvs] Indicate strand specific reads as first read is forward (fwd) or first read is reverse (rvs)'),
                        required=False, default=None)                        
    parser.add_argument('--fragment-length', dest='fragmentlength',
                        help=('Estimated average fragment length. Only required for single end data'),
                        required=False, default=None)                        
    parser.add_argument('--sd', dest='sd',
                        help=('Estimated standard deviation of fragment length. Only required for single end data'),
                        required=False, default=None)                        
    parser.add_argument('--threads', dest='threads',
                        help='Specifies the number of files which can be processed simultaneously.',
                        required=False, default="1")
    parser.add_argument('--plaintext', action='store_true',
                        help=('Output plaintext instead of HDF5'),
                        required=False)                        

    return parser.parse_args(argv)


def kallisto(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = kallisto_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['kallisto']))

    
    #Create outdir
    outDir = args.outdir
    if not re.match('^/',outDir):
        outDir = os.path.abspath(outDir)
        log.log.info('Generated absolute path for outdir as: %s' % outDir)		
    else:
        log.log.info("kallisto output directory set to: %s" % outDir)

    if not os.path.exists(outDir):
        os.makedirs(outDir)
        log.log.info('Created outdir at: %s' % outDir) 
    
    ##Create arguement array
    kallisto_cmd = ['kallisto']
    kallisto_cmd.append('quant')    
    
    kallisto_cmd.append("--index") 
    kallisto_cmd.append(args.index)
    kallisto_cmd.append("--output-dir") 
    kallisto_cmd.append(outDir)

    if not args.nobias:
        kallisto_cmd.append("--bias") 
    
    if args.bootstrapsamples != "0":
        kallisto_cmd.append("--bootstrap-samples")
        kallisto_cmd.append(args.bootstrapsamples)
        kallisto_cmd.append("--seed")
        kallisto_cmd.append(args.seed)
    
    if args.plaintext:
        kallisto_cmd.append("--plaintext")

    if args.fusion:
        kallisto_cmd.append("--fusion")

    if args.stranded is not None:
        if args.stranded.lower() == 'fwd':
            kallisto_cmd.append("--fr-stranded")
        elif args.stranded.lower() == 'rvs':
            kallisto_cmd.append("--rf-stranded")
        else:
            log.log.error("Unrecognised --stranded value: %s" % args.stranded)
            exit(1)

    kallisto_cmd.append("--threads")
    kallisto_cmd.append(args.threads)

    fastQ1 = args.fastq1
    if not re.match('^/',fastQ1):
         fastQ1 = os.path.abspath(fastQ1)

    if args.fastq2 is None:
        if args.fragmentlength is not None and args.sd is not None:
            log.log.info('Only one fastq supplied, running in single end mode') 
            kallisto_cmd.append("--single") 
            kallisto_cmd.append("--sd") 
            kallisto_cmd.append(args.sd)
            kallisto_cmd.append("--fragment-length") 
            kallisto_cmd.append(args.fragmentlength)
            kallisto_cmd.append(fastQ1)
            log.log.info('Using Fastq1: %s' % fastQ1) 
        else:
            log.log.error("When only supplying one fastq file (i.e running in single end mode) you must supply --fragment-length and --sd")
            exit(2)
    else:
        kallisto_cmd.append(fastQ1)
        
        fastQ2 = args.fastq2
        if not re.match('^/',fastQ2):
             fastQ2 = os.path.abspath(fastQ2)
        kallisto_cmd.append(fastQ2)

    log.log.info('Exec Cmd is: %s' %  " ".join(kallisto_cmd))
    log.log.info('Execute kallisto with python subprocess.Popen')
    kallisto_proc = subprocess.Popen(kallisto_cmd, stdout=subprocess.PIPE)
    out0 = kallisto_proc.communicate()[0]
    log.log.info(out0)
    finishFile = os.path.join(outDir,"kallisto.finished")
    touch_process = subprocess.Popen(["touch", finishFile])
    log.log.info('Finished kallisto')
