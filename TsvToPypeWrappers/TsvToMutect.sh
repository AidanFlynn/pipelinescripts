
#!/bin/bash
##Input file format TSV: TumourName NormalName TumFastQ1 TumFastQ2 NormFastQ1 NormFastQ2 outDir
FQrgx="@.+:[0-9]+:([a-zA-Z0-9]+):([0-9]):[0-9]+:[0-9]+:[0-9]+[[:blank:]][12]:[YN]:[0-9]+:([ATCG0-9]+)"
bwaListHeader="--header\t--fq2\t--fq1\t--bam_out\t--tmp_dir"
tempDir="/projects/cu_10027/scratch"
sampleList=$1
scriptOut=$2

echo 'module load pype/0.1.0' > $scriptOut

while read -r tumourName normalName tumFQ1 tumFQ2 normFQ1 normFQ2 outDir
do
        echo "Processing:$tumourName"
        tumFQfiles=($tumFQ1 $tumFQ2)
        normFQfiles=($normFQ1 $normFQ2)

        ##Write tumour bwa list file
        readtag="$(zcat ${tumFQfiles[1]} | grep @ | head -n 1)"
        [[ $readtag =~ $FQrgx ]] || (echo 'Fast Q read tag does not match regex' && exit 1)
        RGID="${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]}"
        tumourRGtag="@RG,ID:$RGID,PL:Illumina,DS:$RGID,SM:$tumourName"
        mkdir -p "$outDir/bam/$tumourName"
        tumourBamOut="$outDir/bam/$tumourName/$tumourName.bam"
        #tumourBwaListOut="$outDir/bam/$tumourName/$tumourName.bwalist.tsv"
        #echo -e $bwaListHeader > $tumourBwaListOut
        #echo -e "$RGtag\t$tumFQ2\t$tumFQ1\t$tumourBamOut\t$tempDir" >>  $tumourBwaListOut

        ##Write normal bwa list file
        readtag="$(zcat ${normFQfiles[1]} | grep @ | head -n 1)"
        [[ $readtag =~ $FQrgx ]] || (echo 'Fast Q read tag does not match regex' && exit 1)
        RGID="${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]}"
        normalRGtag="@RG,ID:$RGID,PL:Illumina,DS:$RGID,SM:$normalName" 
        mkdir -p "$outDir/bam/$normalName"
        normalBamOut="$outDir/bam/$normalName/$normalName.bam"
        #normalBwaListOut="$outDir/bam/$normalName/$normalName.bamheader"
        #echo -e $bwaListHeader >  $normalBwaListOut
        #echo -e "$normalRGtag\t$normFQ2\t$normFQ1\t$normalBamOut\t$tempDir" >  $normalBwaListOut

        ##Write mutect exec cmd
        qc_bam_tumor="$outDir/bam/$tumourName/QC"
        qc_bam_normal="$outDir/bam/$normalName/QC"

        #echo "pype pipelines mutect --normal_bam $normalBamOut --tumor_bam $tumourBamOut --out_dir $outDir --sample_name $tumourName --qc_bam_tumor $qc_bam_tumor --qc_bam_normal $qc_bam_normal --tumor_bwa_list $tumourBwaListOut --normal_bwa_list $normalBwaListOut  --tmp_dir $tempDir" >> $scriptOut
		echo "pype pipelines mutect --tumor_bam $tumourBamOut --out_dir $outDir --sample_name $tumourName --normal_bam $normalBamOut --normal_f1 $normFQ1 --normal_f2 $normFQ2 --header_normal $normalRGtag --tumor_f1 $tumFQ1 --tumor_f2 $tumFQ2 --header_tumor $tumourRGtag" >> $scriptOut
		echo "sleep 1" >> $scriptOut

done < $sampleList
