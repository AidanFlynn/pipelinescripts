#!/bin/bash
##Input file format TSV: TumourName NormalName TumFastQ1 TumFastQ2 NormFastQ1 NormFastQ2 outDir
tempDir="/projects/cu_10027/scratch"
sampleList=$1
scriptOut=$2
J=$3

echo '#!/bin/sh' > $scriptOut

while read -r tumourName normalName tumFQ1 tumFQ2 normFQ1 normFQ2 outDir
do
        #Define outDirs
        qc_tumor="$outDir/bam/$tumourName/QC/FastQC"
        qc_normal="$outDir/bam/$normalName/QC/FastQC"
        
		#Make outDirs
		mkdir -p $qc_tumor
        mkdir -p $qc_normal
        
		##Write fastqc exec cmd
		echo "Writing:$tumourName"
		echo "echo \"module load java/1.8.0 perl/5.24.0 fastqc/0.11.5 && fastqc --quiet --outdir $qc_tumor --extract --threads $J -f fastq $tumFQ1 $tumFQ2\" | qsub -N $tumourName-FastQC -l walltime=1:00:00,mem=1gb,nodes=1:ppn=$J" >> $scriptOut
		echo "sleep 1" >> $scriptOut
		
		echo "Writing:$normalName"
		echo "echo \"module load java/1.8.0 perl/5.24.0 fastqc/0.11.5 && fastqc --quiet --outdir $qc_normal --extract --threads $J -f fastq $normFQ1 $normFQ2\" | qsub -N $normalName-FastQC -l walltime=1:00:00,mem=1gb,nodes=1:ppn=$J" >> $scriptOut
		echo "sleep 1" >> $scriptOut
done < $sampleList

chmod +x $scriptOut